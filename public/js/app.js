$(document).ready(function(){

    $(document).on('click','.genBtn', function(){
        var sizeVal = $('.sizeVal').val()
        var levelVal = $('.levelVal').val()
        $(".main-footer").html('')
        if(sizeVal === '' || levelVal === ''){
            var message = '<div class="alert alert-danger"><strong>Error!</strong> You must choose both options above before to generate a puzzle.</div>'
            if($(".main-footer").html() === ''){
                $(".main-footer").append(message)          
            }
        } else {
            var message = '<div class="alert alert-success"><strong>Success!</strong> You got a new puzzle !</div>'
            if($(".main-footer").html() === ''){
                $(".main-footer").append(message)
            }
        }
    });

    $(document).on("click", ".sizeBtn", function(){
        var mainContainer = $(".btn-container");
        var template = '<button type="button" class="9x9Btn animatedBtn btn btn-outline-primary btn-lg btn-block">9 x 9</button><button type="button" class="6x6Btn animatedBtn btn btn-outline-primary btn-lg btn-block">6 x 6</button><button type="button" class="4x4Btn animatedBtn btn btn-outline-primary btn-lg btn-block">4 x 4</button>'
        mainContainer.html(template)
    });

    $(document).on('click','.diffBtn', function(){
        var sizeVal = $('.sizeVal').val()
        if(sizeVal === ''){
            var message = '<div class="alertMessage alert alert-danger"><strong>Error!</strong> You must choose a board size before select a level.</div>'
            if($(".main-footer").html() === ''){
                $(".main-footer").append(message)
            }
        }else{
            var mainContainer = $(".btn-container");
            var template = '<button type="button" class="vEasyBtn animatedBtn btn btn-outline-primary btn-lg btn-block">Very easy</button><button type="button" class="easyBtn animatedBtn btn btn-outline-primary btn-lg btn-block">Easy</button><button type="button" class="mediumBtn animatedBtn btn btn-outline-primary btn-lg btn-block">Medium</button><button type="button" class="hardBtn animatedBtn btn btn-outline-primary btn-lg btn-block">Hard</button><button type="button" class="vHardBtn animatedBtn btn btn-outline-primary btn-lg btn-block">Very hard</button>'
            mainContainer.html(template)
        }
    });

    $(document).on('click','.9x9Btn',function(e){
        var mainContainer = $(".btn-container");
        var sizeVal = $('.sizeVal');
        sizeVal.val("9x9")
        console.log(sizeVal.val())
        var levelVal = $('.levelVal');
        var levelBtnLabel = getLevelSelected(levelVal.val());
        if("Select level" == levelBtnLabel){
            var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">9x9 Puzzle selected</button><button type="button" class="diffBtn animatedBtn btn btn-outline-primary btn-lg btn-block">Select level</button><button type="button" class="genBtn animatedBtn btn btn-primary btn-lg btn-block">Generate</button>'
        }else{
            var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">9x9 Puzzle selected</button><button type="button" class="diffBtn btn btn-success btn-lg btn-block">'+levelBtnLabel+'</button><input class="animatedBtn btn btn-primary btn-lg btn-block" type="submit" value="Generate">'
        }
        mainContainer.html(template)
    });

    $(document).on('click','.6x6Btn',function(e){
        var mainContainer = $(".btn-container");
        var sizeVal = $('.sizeVal');
        sizeVal.val("6x6")
        console.log(sizeVal.val())
        var levelVal = $('.levelVal');
        var levelBtnLabel = getLevelSelected(levelVal.val());
        if("Select level" == levelBtnLabel){
            var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">6x6 Puzzle selected</button><button type="button" class="diffBtn animatedBtn btn btn-outline-primary btn-lg btn-block">Select level</button><button type="button" class="genBtn animatedBtn btn btn-primary btn-lg btn-block">Generate</button>'
        }else{
            var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">6x6 Puzzle selected</button><button type="button" class="diffBtn btn btn-success btn-lg btn-block">'+levelBtnLabel+'</button><input class="animatedBtn btn btn-primary btn-lg btn-block" type="submit" value="Generate">'
        }
        mainContainer.html(template)
    });

    $(document).on('click','.4x4Btn',function(e){
        var mainContainer = $(".btn-container");
        var sizeVal = $('.sizeVal');
        sizeVal.val("4x4")
        console.log(sizeVal.val())
        var levelVal = $('.levelVal');
        var levelBtnLabel = getLevelSelected(levelVal.val());
        if("Select level" == levelBtnLabel){
            var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">4x4 Puzzle selected</button><button type="button" class="diffBtn animatedBtn btn btn-outline-primary btn-lg btn-block">Select level</button><button type="button" class="genBtn animatedBtn btn btn-primary btn-lg btn-block">Generate</button>'
        }else{
            var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">4x4 Puzzle selected</button><button type="button" class="diffBtn btn btn-success btn-lg btn-block">'+levelBtnLabel+'</button><input class="animatedBtn btn btn-primary btn-lg btn-block" type="submit" value="Generate">'
        }
        mainContainer.html(template)
    });

    $(document).on("click",'.vEasyBtn', function(){
        var mainContainer = $(".btn-container");
        var levelVal = $('.levelVal');
        levelVal.val('very-easy');
        console.log(levelVal.val())
        var sizeVal = $('.sizeVal');
        var sizeBtnLabel = getSizeSelected(sizeVal.val());
        var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">'+sizeBtnLabel+'</button><button type="button" class="diffBtn btn btn-success btn-lg btn-block">Very easy level</button><input class="animatedBtn btn btn-primary btn-lg btn-block" type="submit" value="Generate">'
        mainContainer.html(template)
    });

    $(document).on("click",'.easyBtn', function(){
        var mainContainer = $(".btn-container");
        var levelVal = $('.levelVal');
        levelVal.val('easy');
        console.log(levelVal.val())
        var sizeVal = $('.sizeVal');
        var sizeBtnLabel = getSizeSelected(sizeVal.val());
        var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">'+sizeBtnLabel+'</button><button type="button" class="diffBtn btn btn-success btn-lg btn-block">Easy level</button><input class="animatedBtn btn btn-primary btn-lg btn-block" type="submit" value="Generate">'
        mainContainer.html(template)
    });

    $(document).on("click",'.mediumBtn', function(){
        var mainContainer = $(".btn-container");
        var levelVal = $('.levelVal');
        levelVal.val('medium');
        console.log(levelVal.val())
        var sizeVal = $('.sizeVal');
        var sizeBtnLabel = getSizeSelected(sizeVal.val());
        var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">'+sizeBtnLabel+'</button><button type="button" class="diffBtn btn btn-success btn-lg btn-block">Medium level</button><input class="animatedBtn btn btn-primary btn-lg btn-block" type="submit" value="Generate">'
        mainContainer.html(template)
    });

    $(document).on("click",'.hardBtn', function(){
        var mainContainer = $(".btn-container");
        var levelVal = $('.levelVal');
        levelVal.val('hard');
        console.log(levelVal.val())
        var sizeVal = $('.sizeVal');
        var sizeBtnLabel = getSizeSelected(sizeVal.val());
        var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">'+sizeBtnLabel+'</button><button type="button" class="diffBtn btn btn-success btn-lg btn-block">Hard level</button><input class="animatedBtn btn btn-primary btn-lg btn-block" type="submit" value="Generate">'
        mainContainer.html(template)
    });

    $(document).on("click",'.vHardBtn', function(){
        var mainContainer = $(".btn-container");
        var levelVal = $('.levelVal');
        levelVal.val('very-hard');
        console.log(levelVal.val())
        var sizeVal = $('.sizeVal');
        var sizeBtnLabel = getSizeSelected(sizeVal.val());
        var template = '<button type="button" class="sizeBtn btn btn-success btn-lg btn-block">'+sizeBtnLabel+'</button><button type="button" class="diffBtn btn btn-success btn-lg btn-block">Very hard level</button><input class="animatedBtn btn btn-primary btn-lg btn-block" type="submit" value="Generate">'
        mainContainer.html(template)
    });

    function getLevelSelected(input){
        var label = '';
        switch(input){
            case "":
                label = "Select level";
                break;
            case "very-easy":
                label = "Very easy level";
                break;
            case "easy":
                label = "Easy level";
                break;
            case "medium":
                label = "Medium level";
                break;
            case "hard":
                label = "Hard level";
                break;
            case "very-hard":
                label = "Very hard level";
                break;
        }
        return label;
    }

    function getSizeSelected(input){
        var label = '';
        switch(input){
            case "":
                label = "Board size";
                break;
            case "9x9":
                label = "9x9 Puzzle selected";
                break;
            case "6x6":
                label = "6x6 Puzzle selected";
                break;
            case "4x4":
                label = "4x4 Puzzle selected";
                break;
        }
        return label;
    }
});