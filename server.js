const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

//initialize express app
const app = express();

// Set body parser for parse incoming requests bodies in a middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// View renderizing
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Up server
let port = 3000;

const server = app.listen(port, () => {    
    console.log('Server is up and running on port number ' + port);
});

// Imports routes for the app
app.get('/', (req, res) => {
    res.json({"message": "Welcome to sudoku application. Take a sudoku quickly..."});
});

require('./routes/main.route.js')(app);