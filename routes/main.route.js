const mainController = require('../controllers/main.controller');

module.exports = (app) => {
    app.get('/main',mainController.index);

    app.post('/main/show',mainController.showData);
};